<?php

/**
 * Extension Manager/Repository config file for ext "green_agency".
 */
$EM_CONF[$_EXTKEY] = [
    'title' => 'Green Agency',
    'description' => '',
    'category' => 'templates',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.0-9.5.99',
            'rte_ckeditor' => '8.7.0-9.5.99',
            'bootstrap_package' => '11.0.0-11.9.99'
        ],
        'conflicts' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'Typo3graf\\GreenAgency\\' => 'Classes'
        ],
    ],
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'Typo3graf Themes-Team',
    'author_email' => 'development@typo3graf.de',
    'author_company' => 'Typo3graf media-agentur UG',
    'version' => '1.0.0',
];
